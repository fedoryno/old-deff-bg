DELETE FROM `spell_script_names` WHERE `ScriptName` = 'TW_spell_sha_totem_summon';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES 
(16190, 'TW_spell_sha_totem_summon'), -- Mana Tide
(8512, 'TW_spell_sha_totem_summon'), -- Windfury
(8177, 'TW_spell_sha_totem_summon'), -- Grounding
(8170, 'TW_spell_sha_totem_summon'), -- Cleansing
(8143, 'TW_spell_sha_totem_summon'), -- Tremor
(3738, 'TW_spell_sha_totem_summon'), -- Wrath of Air
(2484, 'TW_spell_sha_totem_summon'), -- Earthbind
(-3599, 'TW_spell_sha_totem_summon'), -- Searing
(-5394, 'TW_spell_sha_totem_summon'), -- Healing Stream
(-5675, 'TW_spell_sha_totem_summon'), -- Mana Spring
(-5730, 'TW_spell_sha_totem_summon'), -- Stoneclaw
(-8071, 'TW_spell_sha_totem_summon'), -- Stoneskin
(-8075, 'TW_spell_sha_totem_summon'), -- Strength of Earth
(-8181, 'TW_spell_sha_totem_summon'), -- Frost Resistance
(-8184, 'TW_spell_sha_totem_summon'), -- Fire Resistance
(-8190, 'TW_spell_sha_totem_summon'), -- Magma
(-8227, 'TW_spell_sha_totem_summon'), -- Flametongue
(-10595, 'TW_spell_sha_totem_summon'), -- Nature Resistance
(-30706, 'TW_spell_sha_totem_summon'); -- Totem of Wrath