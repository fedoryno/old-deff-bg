#include "ScriptPCH.h"

#define MENU 500
#define TITUL 1000
#define MENU_GM 50

enum Barva
{
    BARVA_CERVENA = 8,
    BARVA_CERNA = 7,
    BARVA_TMAVE_SEDA = 6,
    BARVA_ZLATA = 5,
    BARVA_FIALOVA = 4,
    BARVA_MODRA = 3,
    BARVA_ZELENA = 2,
    BARVA_SEDA = 1
};

const std::string Barvy[8] =
{
    "454545", "00ff00", "0000cd", "8a2be2", "ff8c00", "1e1e1e", "000000", "FF0000"
};

struct Odmena
{
    Odmena(uint32 _playerGUID, uint32 _title, uint32 _done)
    {
        playerGUID = _playerGUID; title = _title; done = _done;
    }

    uint32 playerGUID;
    uint32 title;
    uint32 done;
};

typedef std::list<Odmena*> Odmeny;

class OdmenyZaSezonu
{
public:
    static void NactiOdmeny();
    static void AplikujOdmenu(Player* player, Creature* creature, uint32 titul);
    static void VypisOdmenyProGM(Player* player, Creature* creature, uint32 nasobic);
    static void VypisOdmenyProHrace(Player* player);
    static void AplikujOdmenuDB(Player* player, uint32 titul);
    static void AplikujOdmenuCore(Player* player, uint32 titul);
    static void GossipHelloMainMenu(Player* player, Creature* creature);

public:
    static bool isEmpty() { return (odmeny.size() == 0); }
    static bool IsLoaded() { return loaded; }
    static bool loaded;

public:
    static std::string UdelejText(int32 hodnota);
    static std::string BarevnyText(uint32 barva, std::string text);

private:
    static Odmeny odmeny;
};